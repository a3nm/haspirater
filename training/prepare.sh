#!/bin/bash

# Prepare a text for piping into detect.pl

tr -c "a-zA-ZÀ-Ÿà-ÿ\n'-" "\n" | sed "s/'/'\n/"

