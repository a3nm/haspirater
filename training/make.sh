#!/bin/bash

# From a French text input and an exceptions dictionary, prepare the
# trie.

./prepare.sh | # reformat the text
  ./detect.pl | # identify and label occurrences
  cat - $* | # add in exceptions
  sed 's/ h/ /' | # we don't keep the useless leading 'h' in the trie
  ../haspirater/buildtrie.py  | # prepare the trie
  ../haspirater/compresstrie.py | # compress the trie
  ../haspirater/majoritytrie.py # keep only the most frequent information

