#!/usr/bin/perl

# From a list of '\n'-separated words, output occurrences of words
# starting by 'h' when it can be inferred whether the word is aspirated
# or not. The format is "0 word" for non-aspirated and "1 word" for
# aspirated.

my $asp; # will the next word be aspirated?

while (<>) {
  $_ = lc($_);
  print "$asp $_" if (/^h/i && $asp >= 0);
  chop;
  # we store in asp what the current word indicates about the next word
  $asp = -1; # default is unknown
  $asp = 0 if /^[lj]'$/;
  $asp = 0 if /^qu'$/;
  $asp = 1 if /^que$/;
  $asp = 0 if /^cet$/;
  $asp = 1 if /^ce$/;
  # only meaningful are "je", "de", "le" and "la"
  $asp = 1 if /^[jdl][ea]$/;
}

